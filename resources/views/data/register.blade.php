<!DOCTYPE html>
<html lang="en">
<head>
        <title> Form Sign Up </title>
    </head>

    <body>
        <h1> Buat Account Baru! </h1>
        <h3> Sign Up Form </h3>

        <!-- MEMBUAT FORM -->
        <div> 
            <form action="/post" method="POST">
            @csrf
            <label for="namadepan">First Name</label><br>
            <input type="text" name="nama_depan" id="namadepan"><br>
            <label for="namabelakang">Last Name</label><br>
            <input type="text" name="nama_belakang" id="namabelakang"><br>        
        </div>

        <!-- checkbox dan radio -->
   
            <!-- Radio -->
        <div>
            <p> <bold>Gender :</bold></p>
            <input type="radio" name="gender" value="0"> Male <br>
            <input type="radio" name="gender" value="1"> Female <br>
            <input type="radio" name="gender" value="2"> Other
        </div>

            <!-- Select dropdown -->
        <div>
            <p> Nationality:</p>
                <select>
                    <option value="ID"> Indonesian </option>
                    <option value="SG"> Singaporean </option>
                    <option value="MY"> Malaysian </option>
                </select>
        </div>

            <!-- Checkbox -->
        <div>
            <p> Languange spoken:</p>
            <input type="checkbox"> Bahasa Indonesia <br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other <br>
        </div>

            <!-- Text Area -->
            <p> Bio: </p>
            <textarea cols="40" rows="15"></textarea> <br> <br>

            <!-- Tombol Submit -->
            <input type="submit" value="Sign Up">
            

        </form> 
    </body>
</html>